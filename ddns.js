const dns       = require('dns');
const dotenv    = require('dotenv');
const validator = require('validator');
const https     = require('https');

dotenv.load();

const args = process.argv.slice(2);

let config = {
	host : null,
	email: null,
	key  : null,
	ttl  : 3600,
};

Object.keys(config).forEach((arg, argIndex) => {
	if ( args[argIndex] ) {
		config[arg] = args[argIndex];
		return;
	}

	let envArg = `CF_DDNS_${arg.toUpperCase()}`;

	if ( process.env[envArg] ) {
		config[arg] = process.env[envArg];
	}
});

const configErrors = [];

Object.keys(config).forEach((key) => {
	if ( ! config[key] ) {
		configErrors.push( `${key} is missing` );
		return;
	}

	switch( key ) {
		case 'host':
			dns.resolve(config[key], 'A', (err) => {
				if ( null !== err ) {
					configErrors.push(`${config[key]} is an invalid domain`);
				}
			});
		break;
		case 'email':
			if ( ! validator.isEmail( config[key] ) ) {
				configErrors.push(`${config[key]} is an invalid email`);
			}
		break;
	}
});

if ( configErrors.length > 0 ) {
	console.error( config );
	console.error( configErrors.join("\n") );
	process.exit();
}

const zoneDomain = config.host.split('.').length -1 > 1 ? config.host.substr(config.host.indexOf('.') + 1) : config.domain;

const headers = {
	Accept: 'application/json',
	'X-Auth-Email': config.email,
	'X-Auth-Key': config.key,
};

const logErr = function(e) {
	console.error(e.message);
	process.exit();
}

https.get( 'https://api.cloudflare.com/client/v4/zones/', { headers: headers }, (res) => {
	res.setEncoding('utf8');
	let body = '';
	let zones, zone, zoneIndex;

	res.on('data', (data) => {
		body += data;
	});

	res.on('end', () => {
		try {
			zones = JSON.parse(body);
		} catch (e) {
			logErr(e);
		}

		if ( zones.errors && zones.errors.length > 0 ) {
			console.error(zones.errors.join("\n"));
			process.exit();
		}

		zoneIndex = zones.result.map(zone => zone.name).indexOf(zoneDomain);

		if ( zoneIndex === -1 ) {
			logErr( { message: `${zoneDomain} not found in cloudflare zones` } );
		}

		zone = zones.result[zoneIndex];

		https.get( `https://api.cloudflare.com/client/v4/zones/${zone.id}/dns_records`, { headers: headers }, (res) => {
			let body = '';
			let records;

			res.on('data', (data) => {
				body += data;
			});
			res.on('end', () => {
				try {
					records = JSON.parse(body);
				} catch (e) {
					logErr(e);
				}

				if ( records.errors && records.errors.length > 0 ) {
					logErr({ message: records.errors.join("\n") });
				}

				let recordIndex = records.result.map( result => result.name ).indexOf( config.host );

				if ( recordIndex === -1 ) {
					console.error(`${config.host} record not found in ${zoneDomain} dns records`);
					process.exit();
				}

				let record = records.result[ recordIndex ];

				https.get('https://ipinfo.io', (res) => {
					let body = '';
					let ipInfo;

					res.on('data', (data) => {
						body += data;
					});
					res.on('end', () => {
						try {
							ipInfo = JSON.parse(body);
						} catch (e) {
							logErr(e);
						}

						if ( ipInfo.ip === record.content ) {
							console.log( `IP Address unchanged for ${config.host} - ${ipInfo.ip}`);
							process.exit();
						}

						record.content = ipInfo.ip;
						record.ttl     = config.ttl;

						let jsonRecord = JSON.stringify(record);

						const updateRequest = https.request( `https://api.cloudflare.com/client/v4/zones/${zone.id}/dns_records/${record.id}`, {
							method: 'PUT',
							headers: Object.assign( headers, {
								'Content-Type': 'application/json',
								'Content-Length': Buffer.byteLength(jsonRecord)
							}),
						}, (res) => {
							res.setEncoding('utf8');
							let body = '';
							res.on('data', (chunk) => {
								body += chunk;
							});
							res.on('end', () => {
								try {
									updateResponse = JSON.parse(body);
								} catch(e) {
									logErr(e);
								}

								if ( updateResponse.errors.length > 0 ) {
									logErr( { message: updateResponse.errors.map( err => `${err.code} - ${err.message} - ${err.error_chain.map( chain => chain.message ).join(', ')}`).join("\n") });
								}

								if ( updateResponse.success ) {
									console.log( `IP Address updated to ${ipInfo.ip} - ${config.host}` );
									process.exit();
								}

								logErr( { message: 'Unknown error' } );
							});
						}).on('error', logErr);

						updateRequest.write(jsonRecord);
						updateRequest.end();
					});
				}).on('error', logErr);
			});
		}).on('error', logErr);
	});
}).on('error', logErr);
