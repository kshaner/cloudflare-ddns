# Cloudflare DDNS Client

## Requirements

* Node.js installed
* Cloudflare account with API key and domain setup

## Installation

1. clone this repo using `git clone git@gitlab.com:kshaner/cloudflare-ddns.git`
2. run `npm install` in the cloned directory

@TODO, move this package to npm as well

## Usage

`$ node ddns.js <host> <email> <apikey> [<ttl>=3600]`

You can also specify environment variables of in lieu of arguments:

* CF_DDNS_HOST
* CF_DDNS_EMAIL
* CF_DDNS_KEY
* CF_DDNS_TTL

the .env file is supported as well with dotenv

## Daemonize

It is recommended to use environment variables with this setup

### Windows

Create a new task scheduler entry pointing to the path of the ddns.js script with the desired interval

### OSX/Linux

Setup a crontab with the the path to ddns.js path with the desired interval
